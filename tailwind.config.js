module.exports = {
  theme: {
    extend: {
      scale: {
        102: '1.02',
      },
      colors: {
        main: 'var(--main-color)',
        secondary: 'var(--secondary-color)',
        light: 'var(--text-color)',
        dark: 'var(--dark-color)',
        'dark-a10': 'var(--dark-color-a10)',
        'dark-a20': 'var(--dark-color-a20)',
        'dark-a80': 'var(--dark-color-a80)',
        'light-a10': 'var(--text-color-a10)',
        'light-a60': 'var(--text-color-a60)',
        'light-a90': 'var(--text-color-a90)',
        'secondary-a10': 'var(--secondary-color-a10)',
        'secondary-a20': 'var(--secondary-color-a20)',
        'secondary-a60': 'var(--secondary-color-a60)',
        'secondary-a90': 'var(--secondary-color-a90)',
        accent: 'var(--accent-color)',
        'accent-a90': 'var(--accent-color-a90)',
        error: 'var(--error-color)',
        'error-a70': 'var(--error-color-a70)',
        extra: 'var(--extra-color)',
        'extra-a70': 'var(--extra-color-a70)',
      },
      outline: {
        2: '2px solid var(--outline-color)',
        1: '2px solid var(--outline-color)',
      },
      spacing: {
        14: '3.5rem'
      }
    },
  },
  variants: {
    margin: ['responsive', 'hover', 'focus', 'last', 'first'],
    border: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
  },
  plugins: [],
}
