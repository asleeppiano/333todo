import { linear } from 'svelte/easing'
import { styler, delay as popDelay, keyframes, chain, tween } from 'popmotion'

export function slideOff(
  node,
  { direction = 'right', delay = 0, duration = 400, easing = linear }
) {
  const style = getComputedStyle(node)
  const opacity = +style.opacity

  if (window.innerWidth < 1024) {
    return {
      delay,
      duration,
      easing,
      css: (t) =>
        `position: fixed;` +
        `opacity: ${Math.min(t * 20, 1) * opacity};` +
        `transform: translateX(${direction === 'left' ? '-' : ''}${
          (1 - t) * 100
        }%);`,
    }
  }

  return {
    delay,
    duration: direction === 'right' ? duration : 0,
    easing,
    css: (t) => `position:fixed;opacity: ${t * opacity}`,
  }
}

export function cursorAnimation({ cursor, circle, ring1, ring2, ring3, coordinates = { from: {x: 40, y: 40}, to: {x: 130, y: 130}} }) {
  const styledCursor = styler(cursor)
  const styledCircle = styler(circle)
  const styledRing1 = styler(ring1)
  const styledRing2 = styler(ring2)
  const styledRing3 = styler(ring3)

  function ringKeyframes(
    styledRing,
    { duration = 1000, scale = 3, opacity = 1, delay = 1000 }
  ) {
    chain(
      popDelay(delay),
      keyframes({
        values: [{ scale: 1, opacity: 0 }, { scale, opacity }, { opacity: 0 }],
        duration,
      })
    ).start(styledRing.set)
  }
  chain(
    popDelay(500),
    tween({
      from: { x: coordinates.from.x, y: coordinates.from.y },
      to: { x: coordinates.to.x, y: coordinates.to.y },
      duration: 500,
    })
  ).start(styledCursor.set)

  chain(
    popDelay(1000),
    tween({
      from: { scale: 1 },
      to: { scale: 1.2 },
      duration: 1000,
    })
  ).start(styledCircle.set)

  let duration = 1500
  let delay = 1000

  const rings = [styledRing1, styledRing2, styledRing3]
  rings.forEach((ring, i) => {
    duration -= i * 200
    delay += i * 200
    ringKeyframes(ring, {
      duration,
      delay,
      opacity: 0.8,
    })
  })
}
