import { TODO_TYPE, STATE } from '../constants'

function getTodo(id, type, state) {
  if (type === TODO_TYPE.TASK) {
    return {
      id,
      title: '',
      text: {
        html: '',
        content: '',
      },
      done: false,
      state
    }
  } else if (type === TODO_TYPE.ACTIVITY) {
    return {
      id,
      title: '',
      text: {
        html: '',
        content: '',
      },
      timerValues: {
        hours: 0,
        minutes: 1,
        seconds: 0
      },
      initialTimerValues: {
        hours: 0,
        minutes: 1,
        seconds: 0
      },
      sets: 1,
      done: false,
      state
    }
  }
  throw new TypeError('no such todo type')
}

export function getEmptyTodo(id, type) {
  return getTodo(id, type, STATE.EMPTY)
}

export function getBlockedTodo(id, type) {
  return getTodo(id, type, STATE.BLOCKED)
}
