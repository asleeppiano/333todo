import "./css/utils.css"
import View from './view/View.stories.svelte';
import ActiveActivity from '../components/todostate/ActiveActivity.svelte'
import ActiveTask from '../components/todostate/ActiveTask.svelte'
import Empty from "../components/todostate/Empty.svelte";
import { withKnobs } from "@storybook/addon-knobs";
import { STATE } from '../constants'

export default {
  title: "Todo",
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const empty = () => ({
  Component: View,
  props: {
    component: Empty,
    bg: 'main',
    width: 500,
    props: {
      state: STATE.EMPTY,
    }
  }
});

export const filledActivity = () => ({
  Component: View,
  props: {
    component: ActiveActivity,
    bg: 'main',
    width: 500,
    props: {
      todo: {
        title: 'asdf',
        text: {
          html: "hello",
          content: "hello"
        },
        done: false
      },
      type: 'activity',
      state: STATE.FILLED,
      timerValues: {
        hours: 0,
        minutes: 1,
        seconds: 0
      },
      sets: 1
    }
  }
});

export const runningActivity = () => ({
  Component: View,
  props: {
    component: ActiveActivity,
    bg: 'main',
    width: 500,
    props: {
      todo: {
        title: 'asdf',
        text: {
          html: "hello",
          content: "hello"
        },
        done: false
      },
      type: 'activity',
      state: STATE.RUNNING,
      timerValues: {
        hours: 1,
        minutes: 1,
        seconds: 0
      },
      sets: 1
    }
  }
});

export const editActivity = () => ({
  Component: View,
  props: {
    component: ActiveActivity,
    bg: 'main',
    width: 500,
    props: {
      todo: {
        title: 'asdf',
        text: {
          html: "hello",
          content: "hello"
        },
        done: false
      },
      type: 'activity',
      sets: 1,
      timerValues: {
        hours: 0,
        minutes: 1,
        seconds: 0
      },
      state: STATE.EDIT
    }
  }
});

export const editTask = () => ({
  Component: View,
  props: {
    component: ActiveTask,
    bg: 'main',
    width: 500,
    props: {
      todo: {
        title: 'asdf',
        text: {
          html: "hello",
          content: "hello"
        },
        done: false
      },
      type: 'task',
      state: STATE.EDIT
    }
  }
});

export const filledTask = () => ({
  Component: View,
  props: {
    component: ActiveTask,
    bg: 'main',
    width: 500,
    props: {
      todo: {
        title: 'asdf',
        text: {
          html: "hello",
          content: "hello"
        },
        done: false
      },
      type: 'task',
      state: STATE.FILLED
    }
  }
});

