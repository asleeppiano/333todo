import "./css/utils.css"
import View from './view/View.stories.svelte';
import Timer from "../components/Timer.svelte";
import { withKnobs } from "@storybook/addon-knobs";

export default {
  title: "Timer",
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const Default = () => ({
  Component: View,
  props: {
    bg: 'secondary',
    width: 400,
    component: Timer
  }
});

