import "./css/utils.css"
import View from './view/View.stories.svelte';
import Warning from '../components/Warning.svelte';
import { withKnobs } from "@storybook/addon-knobs";

export default {
  title: "Warning",
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const Default = () => ({
  Component: View,
  props: {
    component: Warning,
    bg: 'main',
    width: 500
  }
});
