export const TODO_TYPE = {
  TASK: 'task',
  ACTIVITY: 'activity',
};

export const STATE = {
  EMPTY: 'EMPTY',
  EDIT: 'EDIT',
  GENERATE: 'GENERATE',
  FILLED: 'FILLED',
  RUNNING: 'RUNNING',
  DELETION_MODE: 'DELETION_MODE',
  MARKED: 'MARKED',
  NOT_EDITABLE: 'NOT_EDITABLE',
  DONE: 'DONE',
  ARCHIVED: 'ARCHIVED',
  BLOCKED: 'BLOCKED',
};

export const TIMER_STATE = {
  COUNTDOWN: 'countdown',
  INACTIVE: 'inactive',
  PAUSED: 'paused',
  SETUP: 'setup'
}
