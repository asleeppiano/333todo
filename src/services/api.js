import { get } from 'svelte/store'
import { firebase } from './firebase'
import { userStore } from '../stores/user'

/**
 * @typedef {Object} Todo
 * @property {string} id
 * @property {string} title
 * @property {{html: string, content: string}} text
 * @property {boolean} done
 */

/**
 * @param {string} listype - todo type
 * @returns {Promise < QuerySnapshot < T > >} A Promise that will be resolved with the results of the Query.
 */
export async function getTodos(listtype) {
  const userValues = get(userStore)
  return firebase
    .firestore()
    .collection(
      `users/${userValues.user.email}/sprints/${userValues.data.currentSprint}/${listtype}`
    )
    .get()
}

/**
 * @param {string} littype - todo type
 * @param {Todo} todo
 */
export async function updateTodo(todo, listtype) {
  const userValues = get(userStore)
  try {
    await firebase
      .firestore()
      .collection('users')
      .doc(
        `${userValues.user.email}/sprints/${userValues.data.currentSprint}/${listtype}/${todo.id}`
      )
      .update(todo)
  } catch (e) {
    await firebase
      .firestore()
      .collection('users')
      .doc(
        `${userValues.user.email}/sprints/${userValues.data.currentSprint}/${listtype}/${todo.id}`
      )
      .set(todo)
  }
}

/**
 * @param {string} listtype - todo type
 * @param {string} id - todo id
 */
export async function deleteTodo(listtype, id) {
  const userValues = get(userStore)
  const querySnapshot = await firebase
    .firestore()
    .collection(
      `users/${userValues.user.email}/sprints/${userValues.data.currentSprint}/${listtype}`
    )
  querySnapshot.doc(id).delete()
}

export function getRandomGeneratedTodo(_context , event) {
  console.log('getRandomGeneratedTodo', event.todoType)
  let collectionName = 'generatedTasks'
  const type = event.todoType
  if (type === 'task') {
    collectionName = 'generatedTasks'
  } else if (type === 'activity') {
    collectionName = 'generatedActivities'
  }
  return firebase.firestore().collection(collectionName).get().then(querySnapshot => querySnapshot.docs).catch(e => console.log('firebase error:', e))
}
