import { get } from 'svelte/store'
import { firebase } from './firebase'
import { todosStore } from '../stores/todos'
import { userStore } from '../stores/user'
import { TODO_TYPE, STATE } from '../constants'
import { getTodos, updateTodo, deleteTodo } from './api'

export async function initDB() {
  const userValues = get(userStore)
  const todoValues = get(todosStore)
  if (todoValues.activitylist.length > 0 && todoValues.tasklist.length > 0) {
    return
  }
  if (userValues && userValues.user && userValues.user.GUEST) {
    const storageTodolist = sessionStorage.getItem('tasklist')
    const storageActivitylist = sessionStorage.getItem('activitylist')
    const tasklist = storageTodolist ? JSON.parse(storageTodolist) : []
    const activitylist = storageActivitylist
      ? JSON.parse(storageActivitylist)
      : []
    todosStore.setTodos({ activitylist, tasklist })
    todosStore.fillWithEmpty()
  } else if (userValues && userValues.user && userValues.user.email) {
    const todos = { activitylist: [], tasklist: [] }
    for (const type of Object.values(TODO_TYPE)) {
      const listtype = `${type}list`
      const querySnapshot = await getTodos(listtype)
      querySnapshot.forEach((doc) => {
        if (doc.data().done) {
          todos[listtype].push({ id: doc.id, ...doc.data(), state: STATE.DONE })
        } else {
          todos[listtype].push({
            id: doc.id,
            ...doc.data(),
            state: STATE.FILLED,
          })
        }
      })
    }
    todosStore.setTodos(todos)
    todosStore.fillWithEmpty()
  }
}

export function writeToDB(todo, type) {
  const userValues = get(userStore)
  const listtype = `${type}list`
  if (userValues.user.GUEST) {
    const todolist = sessionStorage.getItem(listtype)
    if (todolist) {
      let parsedList = JSON.parse(todolist)
      if (parsedList.find((item) => item.id === todo.id)) {
        parsedList = parsedList.map((item) => {
          if (item.id === todo.id) {
            return todo
          }
          return item
        })
      } else {
        todo.state = STATE.FILLED
        parsedList = [
          ...parsedList,
          todo
        ]
      }
      sessionStorage.setItem(listtype, JSON.stringify(parsedList))
    } else {
      todo.state = STATE.FILLED
      sessionStorage.setItem(
        listtype,
        JSON.stringify([
          todo
        ])
      )
    }
  } else {
    updateTodo(todo, listtype)
  }
  todosStore.updateTodos(todo, type)
}

export async function deleteFromDB() {
  const userValues = get(userStore)
  if (userValues.user.GUEST) {
    const todosValues = get(todosStore)
    for (const type of Object.values(TODO_TYPE)) {
      const listtype = `${type}list`
      const storageList = sessionStorage.getItem(listtype)
      let parsedList = JSON.parse(storageList)
      if (!parsedList) {
        continue
      }

      todosValues[listtype].forEach((todo) => {
        if (todo.mark) {
          parsedList = parsedList.filter((item) => item.id !== todo.id)
        }
      })

      if (parsedList.length) {
        sessionStorage.setItem(listtype, JSON.stringify(parsedList))
      } else {
        sessionStorage.removeItem(listtype)
      }
    }
  } else {
    const todosValues = get(todosStore)
    for (const type of Object.values(TODO_TYPE)) {
      const listtype = `${type}list`
      for (const todo of todosValues[listtype]) {
        if (todo.mark) {
          await deleteTodo(listtype, todo.id)
        }
      }
    }
  }
  todosStore.deleteMarked()
}

export function checkEndOfSprint(end) {
  const currDate = new Date()
  return end < currDate
}

export async function getUser(email) {
  try {
    const doc = await firebase.firestore().collection('users').doc(email).get()
    if (doc.data()) {
      return doc.data()
    }
    return null
  } catch (e) {
    throw new Error(e)
  }
}

export async function getSprint(email, currentSprint) {
  try {
    const doc = await firebase
      .firestore()
      .collection('users')
      .doc(`${email}/sprints/${currentSprint}`)
      .get()
    if (doc.data()) {
      return doc.data()
    }
    return null
  } catch (e) {
    throw new Error(e)
  }
}

export async function updateCurrentDoneTodos(email, type, id) {
  const userValues = get(userStore)
  try {
    const doc = await firebase
      .firestore()
      .collection('users')
      .doc(`${email}/sprints/${userValues.data.currentSprint}`)
    const docData = await doc.get()
    if (type === 'task') {
      doc.update({
        'task.doneCount': docData.data().task.doneCount + 1,
      })
    } else if (type === 'activity') {
      doc.update({
        'activity.doneCount': docData.data().activity.doneCount + 1,
      })
    }
    const todosDoc = await firebase
      .firestore()
      .collection('users')
      .doc(
        `${email}/sprints/${userValues.data.currentSprint}/${type}list/${id}`
      )
    todosDoc.update({
      done: true,
    })
  } catch (e) {
    throw new Error(e)
  }
}
