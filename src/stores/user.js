import { writable } from 'svelte/store'
import { add } from 'date-fns'
import { firebase } from '../services/firebase'

function createUserStore() {
  const { set, update, subscribe } = writable({
    user: null,
    data: null,
    changed: false,
  })
  return {
    set,
    subscribe,
    setUser: async (user) => {
      if (user && user.GUEST) {
        set({ user: { GUEST: true } })
        return
      }
      const doc = await firebase
        .firestore()
        .collection('users')
        .doc(user.email)
        .get()
      if (!doc.data()) {
        const initialSprintDistance = 1
        const userData = {
          maxTodos: 3,
          currentSprint: 1,
          sprintDistance: initialSprintDistance,
        }
        await firebase
          .firestore()
          .collection('users')
          .doc(user.email)
          .set(userData)

        const start = new Date()
        const end = add(start, { days: initialSprintDistance })
        await firebase
          .firestore()
          .collection('users')
          .doc(`${user.email}/sprints/1`)
          .set({
            start: firebase.firestore.Timestamp.fromDate(start),
            end: firebase.firestore.Timestamp.fromDate(end),
            task: {
              capacity: 3,
              doneCount: 0,
            },
            activity: {
              capacity: 3,
              doneCount: 0,
            },
          })
        update((data) => {
          const newdata = data
          newdata.user = user
          newdata.data = userData
          return newdata
        })
      } else {
        update((data) => {
          const newdata = data
          newdata.user = user
          newdata.data = doc.data()
          return newdata
        })
      }
    },
    changeUserSettings: async ({ mail, sprintDistance = 1, maxTodos = 3 }) => {
      try {
        await firebase.firestore().collection('users').doc(mail).update({
          sprintDistance,
          maxTodos,
        })
        update((data) => {
          const newdata = data
          newdata.data.sprintDistance = sprintDistance
          newdata.data.maxTodos = maxTodos
          return newdata
        })
        return true
      } catch {
        return false
      }
    },
    toggleChanged: () => {
      update((data) => {
        const newdata = data
        newdata.changed = !newdata.changed
        return newdata
      })
    },
    removeUser() {
      set({ user: null, data: null, changed: false })
    },
  }
}

export const userStore = createUserStore()
