import { writable, get } from 'svelte/store'
import { firebase } from '../services/firebase'
import { userStore } from './user'


function createSprintStore() {
  const { set, subscribe, update } = writable({
    task: null,
    activity: null,
    end: null,
    start: null,
  })

  return {
    set,
    subscribe,
    setSprint: async () => {
      const userValues = get(userStore)
      console.log('userValues', userValues)
      if(!userValues.data || !userValues.user) {
        return
      }
      const doc = await firebase
        .firestore()
        .collection(`users/${userValues.user.email}/sprints`)
        .doc(`${userValues.data.currentSprint}`)
        .get()

      if (doc.data()) {
        const { activity, task, end, start } = doc.data()
        const endDate = new firebase.firestore.Timestamp(
          end.seconds,
          end.nanoseconds
        ).toDate()
        const startDate = new firebase.firestore.Timestamp(
          start.seconds,
          start.nanoseconds
        ).toDate()
        set({ activity, task, end: endDate, start: startDate })
      }
    },
    updateSprint: async (sprint) => {
      const userValues = get(userStore)
      const updateObject = {}
      Object.entries(sprint).forEach(([k, v]) => {
        if (v) {
          updateObject[k] = v
        }
      })
      await firebase
        .firestore()
        .collection('users')
        .doc(`${userValues.user.email}/sprints/${userValues.data.currentSprint}`)
        .update({ ...updateObject })

      update((data) => {
        const newdata = data
        Object.entries(updateObject).forEach(([k, v]) => {
          newdata[k] = v
        })
        return newdata
      })
    },
  }
}

export const sprintStore = createSprintStore()
