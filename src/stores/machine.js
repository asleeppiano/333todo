import { writable } from 'svelte/store'
import { createMachine } from 'xstate'
import { STATE, TIMER_STATE } from '../constants'
import { getRandomGeneratedTodo } from '../services/api'

// TODO separate actions and guards
export function initTodoMachine(initialState, id, actions, guards) {
  const {
    save,
    mark,
    unmark,
    done,
    toggleOverlay,
    generate,
    pause,
    finishEdit,
    generateLocally,
  } = { ...actions }
  const { isFilled, isGuest } = { ...guards }

  return createMachine(
    {
      id,
      initial: initialState,
      states: {
        [STATE.EMPTY]: {
          on: {
            EDIT: {
              target: STATE.EDIT,
              actions: ['toggleOverlay'],
            },
            GENERATE: [
              {
                target: STATE.FILLED,
                cond: isGuest,
                actions: 'generateLocally',
              },
              {
                target: STATE.GENERATE,
              },
            ],
            ENABLE_DELETION_MODE: {
              target: STATE.NOT_EDITABLE,
            },
            GOTO_FILLED: {
              target: STATE.FILLED,
            },
            GOTO_DONE: {
              target: STATE.DONE,
            },
            GOTO_BLOCKED: {
              target: STATE.BLOCKED,
            },
          },
        },
        [STATE.NOT_EDITABLE]: {
          on: {
            CONFIRM: {
              target: STATE.EMPTY,
            },
            CANCEL: {
              target: STATE.EMPTY,
            },
          },
        },
        [STATE.EDIT]: {
          on: {
            FILL: [
              {
                target: STATE.FILLED,
                cond: isFilled,
                actions: ['save', 'finishEdit', 'toggleOverlay'],
              },
              {
                target: STATE.EMPTY,
                actions: ['toggleOverlay'],
              },
            ],
            EMPTY: {
              target: STATE.EMPTY,
            },
          },
        },
        [STATE.GENERATE]: {
          invoke: {
            src: 'getRandomGeneratedTodo',
            onDone: {
              target: STATE.FILLED,
              actions: 'generate',
            },
            onError: {
              target: STATE.EMPTY,
            },
          },
        },
        [STATE.FILLED]: {
          on: {
            ENABLE_DELETION_MODE: {
              target: STATE.DELETION_MODE,
            },
            DONE: {
              target: STATE.DONE,
              actions: ['done'],
            },
            EDIT: {
              target: STATE.EDIT,
              actions: ['toggleOverlay'],
            },
            START: {
              target: STATE.RUNNING,
            },
          },
        },
        [STATE.RUNNING]: {
          on: {
            STOP: {
              target: STATE.FILLED,
              actions: ['pause'],
            },
          },
        },
        [STATE.DELETION_MODE]: {
          on: {
            MARK: {
              target: STATE.MARKED,
              actions: ['mark'],
            },
            CONFIRM: {
              target: STATE.FILLED,
            },
            CANCEL: [
              {
                target: STATE.FILLED,
                cond: isFilled,
              },
            ],
          },
        },
        [STATE.MARKED]: {
          on: {
            CANCEL: {
              target: STATE.FILLED,
              cond: isFilled,
              actions: ['unmark'],
            },
            UNMARK: {
              target: STATE.DELETION_MODE,
              actions: ['unmark'],
            },
            CONFIRM: {
              target: STATE.EMPTY,
            },
          },
        },
        [STATE.DONE]: {
          on: {
            ARCHIVE: STATE.ARCHIVED,
            UNDONE: STATE.FILLED,
          },
        },
        [STATE.ARCHIVED]: {
          type: 'final',
        },
        [STATE.BLOCKED]: {},
      },
    },
    {
      actions: {
        save,
        mark,
        unmark,
        done,
        toggleOverlay,
        generate,
        pause,
        finishEdit,
        generateLocally,
      },
      guards: {
        isFilled,
        isGuest,
      },
      services: {
        getRandomGeneratedTodo,
      },
    }
  )
}

function createMachineStore() {
  const { update, subscribe } = writable({
    todoServices: {},
    serviceStates: {}
  })
  return {
    subscribe,
    updateServicesState: (id, state) =>
      update((data) => {
        data.serviceStates[id] = state
        return data
      }),
    addTodoService: (id, service) =>
      update((data) => {
        data.todoServices[id] = service
        return data
      }),
  }
}

export const machineStore = createMachineStore()

export function initTimerMachine(initialState, id, actions) {
  const { run, stop, pause, continueTimer, setup } = { ...actions }
  return createMachine(
    {
      id,
      initial: initialState,
      states: {
        [TIMER_STATE.INACTIVE]: {
          on: {
            START: {
              target: TIMER_STATE.COUNTDOWN,
              actions: 'run',
            },
            SETUP: {
              target: TIMER_STATE.SETUP,
            },
          },
        },
        [TIMER_STATE.COUNTDOWN]: {
          on: {
            PAUSE: {
              target: TIMER_STATE.PAUSED,
              actions: 'pause',
            },
            FINISH: {
              target: TIMER_STATE.INACTIVE,
              actions: 'stop',
            },
          },
        },
        [TIMER_STATE.PAUSED]: {
          on: {
            START: {
              target: TIMER_STATE.COUNTDOWN,
              actions: 'continueTimer',
            },
            FINISH: {
              target: TIMER_STATE.INACTIVE,
              actions: 'stop',
            },
          },
        },
        [TIMER_STATE.SETUP]: {
          on: {
            DONE: {
              target: TIMER_STATE.INACTIVE,
              actions: 'setup',
            },
          },
        },
      },
    },
    {
      actions: {
        run,
        pause,
        stop,
        continueTimer,
        setup,
      },
    }
  )
}

function createTimerMachineStore() {
  const { update, subscribe } = writable({
    timerServices: {},
    serviceStates: {},
  })
  return {
    subscribe,
    updateServicesState: (id, state) =>
      update((data) => {
        data.serviceStates[id] = state
        return data
      }),
    addTimerService: (id, service) =>
      update((data) => {
        data.timerServices[id] = service
        return data
      }),
  }
}

export const timerMachineStore = createTimerMachineStore()
