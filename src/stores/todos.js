import { writable, get, derived } from 'svelte/store'
import { nanoid } from 'nanoid'
import { STATE, TODO_TYPE } from '../constants'
import { sprintStore } from './sprint'
import { userStore } from './user'
import { getEmptyTodo, getBlockedTodo } from '../utils/getTodo'

function createTodosStore() {
  const { set, subscribe, update } = writable({
    tasklist: [],
    activitylist: [],
  })

  return {
    set,
    subscribe,
    setTodos: ({ activitylist = [], tasklist = [] } = {}) =>
      set({ tasklist, activitylist }),
    updateTodos: async (todo, type) => {
      const listtype = `${type}list`
      update((data) => {
        const newdata = data
        newdata[listtype] = newdata[listtype].map((item) => {
          if (item.id === todo.id) {
            if (!todo.state) {
              todo = { ...todo, state: todo.done ? STATE.DONE : STATE.FILLED }
            }
            return todo
          }
          return item
        })
        return newdata
      })
    },
    toggleMark: (type, place) => {
      update((data) => {
        const newdata = data
        newdata[`${type}list`][place].mark = !newdata[`${type}list`][place].mark
        return newdata
      })
    },
    deleteMarked: () => {
      for (const type of Object.values(TODO_TYPE)) {
        const listtype = `${type}list`
        update((data) => {
          data[listtype] = data[listtype].map((todo) => {
            if (todo.mark) {
              return getEmptyTodo(todo.id, type)
            }
            return todo
          })
          return data
        })
      }
    },

    removeTodos: () => set({ tasklist: [], activitylist: [] }),

    // FIXME fix this shit
    fillWithEmpty: () => {
      const userValues = get(userStore)
      if (!userValues.data && !userValues.user) {
        return
      }
      if (userValues.user.GUEST) {
        update((data) => {
          const newdata = data || []
          Object.values(TODO_TYPE).forEach((type) => {
            const listtype = `${type}list`
            const currentLength = newdata[listtype].length
            for (let i = 0; i < 3 - currentLength; i += 1) {
              newdata[listtype].push(getEmptyTodo(nanoid(), type))
            }
          })
          return newdata
        })
        return
      }
      const sprintValues = get(sprintStore)
      update((data) => {
        Object.values(TODO_TYPE).forEach((type) => {
          const listtype = `${type}list`
          let currentLength = data[listtype].length
          console.log('data', data)
          for (
            let i = 0;
            i < sprintValues[type].capacity - currentLength;
            i += 1
          ) {
            data[listtype].push(getEmptyTodo(nanoid(), type))
          }
          currentLength = data[listtype].length
          for (let i = 0; i < userValues.data.maxTodos - currentLength; i += 1)
            data[listtype].push(getBlockedTodo(nanoid(), type))
        })
        return data
      })
    },
  }
}
export const todosStore = createTodosStore()
