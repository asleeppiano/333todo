describe('logged in', () => {

  before(() => {
    cy.login()
    // clear db emulator
    fetch('http://localhost:8080/emulator/v1/projects/todo-171a6/databases/(default)/documents', {
      method: "DELETE"
    })
  })

  beforeEach(() => {
    cy.visit('localhost:5000')
    cy.wait(1000)
  })

  it('user logged in', () => {
    cy.get('header a[href="profile"]').contains('test@mail.test')
  })

  it('user on todos page', () => {
    cy.get('h2:first').contains('Список дел')
  })

  it('can go to profile page', () => {
    cy.contains('test@mail.test').click()
    cy.wait(600)
    cy.contains('Профиль')
  })

  it('can fill all empty todos', () => {
    for (let i = 0; i < 6; i += 1) {
      cy.get('div.empty:first').click()
      cy.focused().type('title')
      cy.get('div.content[contenteditable="true"]').type('content')
      cy.get('[data-testid="overlay"]').click()
      cy.wait(200)
    }
    cy.get('div.empty').should('not.exist')
  })

  it('can done all todos', () => {
    cy.get('div.empty').should('not.exist')

    for (let i = 0; i < 6; i += 1) {
      cy.get('div.filled button.done:first').click()
    }
    
    cy.get('div.done').should('have.length', 6)
  })

})
