import fetch from 'node-fetch'

describe('profile', () => {

  before(() => {
    cy.login()
    // clear db emulator
    fetch('http://localhost:8080/emulator/v1/projects/todo-171a6/databases/(default)/documents', {
      method: "DELETE"
    })
  })

  beforeEach(() => {
    cy.visit('localhost:5000')
    cy.wait(3000)
    cy.contains('test@mail.test').click()
    cy.wait(600)
  })

  it('can change todos count', () => {
    cy.get('#profile-max-todos').clear().type('4')
    cy.wait(100)
    cy.contains('Подтвердить изменения').click()
    cy.contains('333Todo').click()

    cy.get('div.todo').should('have.length', 8)
  })

})
