describe('login form', () => {

  before(() => {
    cy.logout()
  })

  beforeEach(() => {
    cy.visit('localhost:5000')
    cy.wait(1000)
    cy.contains('Начать').click()
    cy.wait(600)
    cy.contains('Пропустить').click()
    cy.wait(600)
  })

  it('shows error when password is too weak', () => {
    cy.get('input[type="email"]').type('bbbb@mail.ru')
    cy.get('input#login-password').type('qwerty')
    cy.get('input#login-repeated-password').type('qwerty')
    cy.contains('Создать аккаунт').click()
    cy.wait(1000)
    cy.get('[data-testid="signup-warning"] div').contains('пароль должен содержать не менее 8 символов, иметь хотя бы одну заглавную букву и одну цифру')
    cy.contains('не валиден').should('not.have.class', 'hidden')
  })

  it("it doesn't show errors if password is valid", () => {
    cy.get('input[type="email"]').type('bbbbmail.ru')
    cy.get('input#login-password').type('qwertyU1')
    cy.get('input#login-repeated-password').type('qwertyU1')
    cy.contains('Создать аккаунт').click()
    cy.wait(1000)
    cy.get('label[for="login-password"] + span').should('have.class', 'hidden')
  })

  it('shows validation error when email is not valid', () => {
    cy.get('input[type="email"]').type('bbbbmail.ru')
    cy.get('input#login-password').type('qwerty')
    cy.get('input#login-repeated-password').type('qwerty')
    cy.contains('Создать аккаунт').click()
    cy.contains('это не имеил').should('not.have.class', 'hidden')
  })
  it('shows sign in form', () => {
    cy.contains('войти').click()
    cy.get('h1').contains('Вход')
  })
  it('shows sign up form', () => {
    cy.contains('войти').click()
    cy.contains('Зарегистрироваться').click()
    cy.get('h1').contains('Регистрация')
  })
  it('shows error when email and password are not valid', () => {
    cy.contains('войти').click()
    cy.get('input[type="email"]').type('aamail.ru')
    cy.get('input#login-password').type('qwerty')
    cy.contains('Войти').click()
    cy.get('label[for="login-email"] + span').should('not.have.class', 'hidden')
    cy.get('label[for="login-password"] + span').should('not.have.class', 'hidden')
  })
})
