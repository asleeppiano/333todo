describe('session storage', () => {
  before(() => {
    cy.logout()
  })

  beforeEach(() => {
    cy.visit('localhost:5000')
    cy.wait(1000)
    cy.contains('Начать').click()
    cy.wait(600)
  })

  it("should show all svgs", () => {
    cy.get('#stage1-svg')
    cy.wait(2500)
    cy.contains('Это название')
    cy.contains('Это текст')
    cy.contains('Дальше').click()
    cy.wait(600)

    cy.get('#stage2-svg')
    cy.contains('Дальше').click()
    cy.wait(600)

    cy.get('#stage3-svg')
    cy.wait(1000)
    cy.contains('ГОТОВО')
    cy.contains('Дальше').click()
    cy.wait(600)

    cy.get('#stage4-svg')
    cy.contains('ЗАБЛОКИРОВАНО')
  })

  it('can move to last tutorial and back to first', () => {
    cy.contains('Дальше').click()
    cy.wait(600)
    cy.contains('Дальше').click()
    cy.wait(600)
    cy.contains('Дальше').click()
    cy.wait(600)

    cy.get('#stage4-svg')

    cy.contains('Назад').click()
    cy.wait(600)
    cy.contains('Назад').click()
    cy.wait(600)
    cy.contains('Назад').click()
    cy.wait(600)

    cy.get('#stage1-svg')
  })

  it('should not display back button in first tutorial', () => {
    cy.get('Назад').should('not.exist')

    cy.contains('Дальше').click()
    cy.wait(600)
    cy.contains('Назад').click()
    cy.wait(600)

    cy.get('Назад').should('not.exist')
  })

  it('should not display skip link in last tutorial', () => {
    cy.contains('Дальше').click()
    cy.wait(600)
    cy.contains('Дальше').click()
    cy.wait(600)
    cy.contains('Дальше').click()
    cy.wait(600)

    cy.get('Пропустить').should('not.exist')
  })
})
