describe('session storage', () => {
  before(() => {
    cy.logout()
  })

  beforeEach(() => {
    cy.visit('localhost:5000')
    cy.wait(1000)
    sessionStorage.clear()
    cy.contains('Начать').click()
    cy.wait(600)
    cy.contains('Пропустить').click()
    cy.wait(600)
    cy.contains('Не хочу регистрироваться').click()
    cy.wait(600)
  })
  it('should do not change sign in button', () => {
    cy.get('a[href="login"]').contains('Войти')
  })
  it('should have right headers', () => {
    cy.get('h1').contains('Список дел')
    cy.get('h1').contains('Список активностей')
  })
  it('should show three empty activities and three empty tasks', () => {
    cy.get('[data-testid="todolist-activity"]').children().should('have.length', 3)
    cy.get('[data-testid="todolist-task"]').children().should('have.length', 3)
  })
  it('should edit todo', () => {
    cy.get('div.empty:first').click()
    cy.focused().type('title')
    cy.get('div.content[contenteditable="true"]').type('content')
    cy.get('[data-testid="overlay"]').click()
    cy.get('div.filled h2[contenteditable="false"]').contains('title')
    cy.get('div.filled div.content[contenteditable="false"]').contains('content')
  })
  it('should delete todo', () => {
    cy.get('div.empty:first').click()
    cy.focused().type('title')
    cy.get('div.content[contenteditable="true"]').type('content')
    cy.get('[data-testid="overlay"]').click()
    cy.get('div.filled h2[contenteditable="false"]').contains('title')
    cy.get('div.filled div.content[contenteditable="false"]').contains('content')

    cy.get('button[data-testid="delete-button"]').click()
    cy.get('div.deletion-mode:first').click()
    cy.get('button[data-testid="confirm-deletion-button"]').click()

    cy.get('div.empty').should('have.length', 6)
  })
  it('should persist todo', () => {
    cy.get('div.empty:first').click()
    cy.focused().type('title')
    cy.get('div.content[contenteditable="true"]').type('content')
    cy.get('[data-testid="overlay"]').click()

    cy.visit('localhost:5000')

    cy.wait(1000)
    cy.contains('Начать').click()
    cy.wait(600)
    cy.contains('Пропустить').click()
    cy.wait(600)
    cy.contains('Не хочу регистрироваться').click()
    cy.wait(600)

    cy.get('div.filled h2[contenteditable="false"]').contains('title')
    cy.get('div.filled div.content[contenteditable="false"]').contains('content')

    cy.get('div.empty').should('have.length', 5)
  })

  it('should fill all empty todos', () => {
    for (let i = 0; i < 6; i += 1) {
      cy.get('div.empty:first').click()
      cy.focused().type('title')
      cy.get('div.content[contenteditable="true"]').type('content')
      cy.get('[data-testid="overlay"]').click()
    }
    cy.get('div.empty').should('not.exist')
  })

  it('should done all filled todos', () => {
    for (let i = 0; i < 6; i += 1) {
      cy.get('div.empty:first').click()
      cy.focused().type('title')
      cy.get('div.content[contenteditable="true"]').type('content')
      cy.get('[data-testid="overlay"]').click()
    }
    cy.get('div.empty').should('not.exist')

    for (let i = 0; i < 6; i += 1) {
      cy.get('div.filled button.done:first').click()
    }
    
    cy.get('div.done').should('have.length', 6)
  })
})
