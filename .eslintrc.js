module.exports = {
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module',
  },
  env: {
    es6: true,
    browser: true,
    node: true,
    'jest/globals': true,
    'cypress/globals': true,
  },
  plugins: ['svelte3', 'cypress'],
  overrides: [
    {
      files: ['**/*.svelte'],
      processor: 'svelte3/svelte3',
    },
    {
      files: ['**/*.test.js'],
      env: {
        jest: true, // now **/*.test.js files' env has both es6 *and* jest
      },
      plugins: ['jest'],
      rules: {
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error',
      },
    },
  ],
  rules: {
    'import/no-mutable-exports': 0,
    'import/first': 0,
    'import/no-duplicates': 0,
    'import/no-unresolved': 0,
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': 0,
    'keyword-spacing': [2, { before: true, after: true }],
    'space-before-blocks': [2, 'always'],
    'no-mixed-spaces-and-tabs': [2, 'smart-tabs'],
    'no-cond-assign': 0,
    'no-unused-vars': 2,
    'object-shorthand': [2, 'always'],
    'no-const-assign': 2,
    'no-class-assign': 2,
    'no-this-before-super': 2,
    'no-var': 2,
    'no-unreachable': 2,
    'valid-typeof': 2,
    'quote-props': [2, 'as-needed'],
    'one-var': [2, 'never'],
    'prefer-arrow-callback': 2,
    'prefer-const': [2, { destructuring: 'all' }],
    'arrow-spacing': 2,
    'no-inner-declarations': 0,
    'require-atomic-updates': 0,
  },
  settings: {
    'import/core-modules': ['svelte'],
    'svelte3/compiler': (() => {
      try {
        return require('svelte/compiler')
      } catch (e) {
        return null
      }
    })(),
  },
  extends: [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'prettier',
  ],
}
