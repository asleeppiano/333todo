const fs = require('fs');
const path = require('path');
const camelCase = require('camelcase');
const svg2js = require("svgo/lib/svgo/svg2js");

const ICON_DIR = path.resolve(__dirname, '../icons');
const ICON_COMPONENTS_DIR = path.resolve(__dirname, "../src/components/icons");

const dir = fs.readdirSync(ICON_DIR)

const iconComponent = (svg, width, height) => `<script>
  export let className = '';
  export let height = '';
  export let width = '';
</script>
<${svg.elem} class={className} width={width ? width : '${width}'} height={height ? height : '${height}'} ${formatAttributes(svg.attrs)}>
  ${svg.content.map(generateElement).join("\n")}
</${svg.elem}>`;

dir.forEach(file => {
  const content = fs.readFileSync(path.resolve(ICON_DIR, file))
  const filename = file.split('-')[0]
  const camelCasedFilename = camelCase(filename) + 'Icon';
  svg2js(content, ({ error, ...rest }) => {
    if (error) {
      console.log(error);
      process.exit(1);
    }
    const svg = rest.content[0];
    const convertedSvg = convert(svg);
    const viewBox = convertedSvg.attrs.viewBox.split(' ')
    fs.writeFileSync(
      path.resolve(ICON_COMPONENTS_DIR, `${camelCasedFilename}.svelte`),
      iconComponent(convertedSvg, viewBox[2], viewBox[3])
    )
  });
})

function formatAttributes(attrs) {
  if (attrs) {
    return Object.keys(attrs).reduce((acc, key, index) => {
      const attribute = `${key}="${attrs[key]}"`;
      if (attribute.includes("width") || attribute.includes("height")) {
        return acc;
      }
      if (index === 0) {
        return attribute;
      }
      return acc + " " + attribute;
    }, "");
  }
  return "";
}

function normalizeAttributes(attrs) {
  if (!attrs) {
    return "";
  }
  const normalizedAttrs = {};
  Object.entries(attrs).forEach(([k, v]) => {
        normalizedAttrs[k] = v["value"];
  });
  return normalizedAttrs;
}



function convert(svg) {
  if (svg.elem === "style") {
    return;
  }
  const icon = {
    elem: svg.elem,
    attrs: normalizeAttributes(svg.attrs),
  };
  if (svg.content) {
    icon.content = svg.content.map(convert);
  }
  return icon;
}

function generateElement(svg) {
  if (svg && (svg.elem === "svg" || svg.elem === "g")) {
    return `<${svg.elem} ${formatAttributes(svg.attrs)} >
      ${svg.content && svg.content.map(generateElement).join("\n")}
    </${svg.elem}>`;
  }
  if (svg) return `<${svg.elem} ${formatAttributes(svg.attrs)}/>`;
}
