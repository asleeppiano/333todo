const path = require('path')

module.exports = ({ config, mode }) => {
  console.log('config', config)
  config.node = {
    fs: false,
    child_process: false,
    module: false
  }
  config.module.rules.push(
    {
      test: /\.css$/,
      loaders: [
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
            config: {
              path: './.storybook/',
            },
          },
        },
      ],

      include: path.resolve(__dirname, '../storybook/'),
    },
    //This is the new block for the addon
    {
      test: /\.stories\.js?$/,
      loaders: [require.resolve('@storybook/source-loader')],
      include: [path.resolve(__dirname, '../storybook')],
      enforce: 'pre',
    },
  )

  return config
}
