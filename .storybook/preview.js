import { addParameters } from '@storybook/svelte';
import {
  INITIAL_VIEWPORTS,
} from '@storybook/addon-viewport';
import '../public/global.css';

const customViewports = {
  LaptopHD: {
    name: 'Laptop - hd',
    styles: {
      width: '1366px',
      height: '768px',
    },
  },
};

addParameters({
    viewport: {
    viewports: {
      ...INITIAL_VIEWPORTS,
      ...customViewports,
    },
  },
});
